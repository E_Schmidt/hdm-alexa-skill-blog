---
title: Building an Alexa Skill using State of the Art Technology
author: Eric Schmidt
Date: December 04, 2016
---

# Building an HdM Alexa Skill - Part I

![[Vladgrin\@Shutterstock](http://www.shutterstock.com/gallery-803866p1.html)](media/chatbot.jpg)

## Introduction to the Project

It is becoming obvious that chatbots are quickly emerging to the next *Big Thing*. This is especially evident by the recent *flood* of publication of software development kits by major tech companies to encourage developers to build applications for their ecosystem.

Microsoft offers a SDK to enable [chatbots in Skype][1], Facebook is implementing chatbots into its own [messenger][2], Google is opening up their [voice assistant][3] to the public and Amazon already offers more than [3.000 custom skills][4] for their Alexa voice service.
Combined with Smart Speaker - take Amazon Echo, Google Home or the very [recently announced device integration of Cortana by Microsoft](5) as an example - seamless voice interaction is introduced into our every day life and opens up a whole new market for developers to implement their products into the daily routine of their users.

To gain expertise with this new technology we decided to develop our own voice service. Since - as of December 2016 - Amazon is slowly releasing their Smart Speakers Echo and Dot to Germany we took the opportunity to be one of the first to release a German speaking *Skill*.

![Information Display of the Media University Stuttgart](media/information_wall.jpg)

Now that we knew **how to** implement the project we needed to decide on **what to** implement. It was important to us that we don't just implement a product that only serves us as a mean to learn and study but something that benefits others as well. The idea for a lightweight knowledge system which covers the basic functions to get through a regular university day at the HdM seemed fitting since we personally struggled with ever changing lecture halls. While there are possibilities to quickly lookup important updates using the [mobile application][6] or one of the information displays which are distributed throughout the buildings, our team took the opportunity to further simplify the daily student life while using as much state of the art technology as possible.

The video below shows an overview over the functionality of the finished project to give a brief preview on what to expect.

![Showcase](media/movie.mp4)

## Task Overview & Team Setup

If we were to start a new project from scratch, we wanted to develop it using both new or sophisticated software tools (e.g. serverless computing).
At the same time, we faced a challenge concerning the hardware implementation: At the time of writing, the Amazon Echo / Dot devices are only available by invitation (and Amazon is very sparse giving them away). So, to be more independent, we initially decided to build our own Alexa powered device using a Raspberry Pi - which led to a whole other problem (more in the corresponding blog post). While two of our group worked on the Raspberry Pi - and later joined the skill development - Jonas and myself wrote the actual skill using Node.js while heavily relying on test driven development. To be able to seamlessly and automatically integrate our code from [Bitbucket](7) into Amazon Lambda we settled for Jenkins which was setup and configured by Malte.


| Team Member         | Activity                                 				|
| ------------------- | -------------------------------------------- 			|
| Andreas Fliehr      | Alexa Implementation on Raspberry Pi & Skill Development|
| Jos� Egas           | Alexa Implementation on Raspberry Pi & Skill Development|
| Eric Schmidt        | Skill Development                        				|
| Jonas Scheffner     | HdM API Wrapper & Skill Development      				|
| Malte Vollmerhausen | Jenkins                                  				|

Distribution of Labour

### Obtaining the Data: Writing a Client Application

Before we could start to write the actual skill we needed to gain access to the relevant information from various sources (The website of the university, menu schedule etc.). Fortunately for us, there is an API available which already parses the data of these sources and returns a JSON-Object containing the information.
Jonas wrote a straightforward wrapper module for this API using Node.js which we later used to obtain the information we needed (The module is available on [npm](https://www.npmjs.com/package/hdm-client)). After the development on the module was completed, we were able to start with the actual development.

## Coming Up

The following blog posts include in-depth coverage of the implementation techniques and reasoning of using Amazon's Alexa Voice Service (AVS), Serverless Computing with Amazon Lambda, Jenkins as the continuous integration platform and Node.js as the programming environment all while heavily relying on test driven development. We hope you find the upcoming articles informative and that we could give you a little insight into our way of working.

Remark: We plan to publish the skill and make it accessible through the amazon skill store! We will update this article as soon as it is available.

## Resources

| Resource Name 		| Link 												|
| -------------------  	| ---------------------------------------------		|
| Skill Repository 		| [https://bitbucket.org/jscheffner/hdm-alexa-skill][7] 	|
| Client Repository 	| [https://bitbucket.org/jscheffner/hdm-node-client][8]	|


[1]: https://blogs.technet.microsoft.com/microsoft_presse/schichtwechsel-skype-bots/
[2]: http://t3n.de/news/f8-facebook-mark-zuckerberg-696459/
[3]: https://developers.google.com/voice-actions/
[4]: https://techcrunch.com/2016/09/13/amazons-alexa-app-store-hits-3000-skills-up-from-1000-in-june/
[5]: https://blogs.windows.com/buildingapps/2016/12/13/cortana-skills-kit-cortana-devices-sdk-announcement/#51s1t7S1pPKFThPF.97
[6]: https://play.google.com/store/apps/details?id=de.hdmstuttgart.mi.hdmmobile
[7]: https://bitbucket.org/jscheffner/hdm-alexa-skill
[8]: https://bitbucket.org/jscheffner/hdm-node-client
